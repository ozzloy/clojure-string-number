;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

;; run from clojure-string-number/
;; clj -M --main fyi.tuwo.string-numbers

(ns fyi.tuwo.string-numbers
  (:gen-class))

(defn add-comma-every-three-elements [coll]
  (->> coll
       (partition-all 3)
       (interpose ",")
       (flatten)))

;; (explode 123) -> [3 2 1]
(defn -explode [integer result]
  (if (zero? integer)
    (if (empty? result) [0] result)
    (-explode (quot integer 10) (conj result (mod integer 10)))))
(defn explode [integer]
  (-explode integer []))

(defn to-string [integer]
  (apply str
         (if (nat-int? integer) "" "-")
         (-> integer
             Math/abs
             explode
             add-comma-every-three-elements
             reverse)))

(defn -main [& args]
  (println (=
            (map to-string
                 [0 1 12 123 1234 -1 -12 -123 -1234])
            '("0" "1" "12" "123" "1,234" "-1" "-12" "-123" "-1,234"))))

;; This file is part of b1ng0.

;; b1ng0 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; b1ng0 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with b1ng0.  If not, see <http://www.gnu.org/licenses/>.
